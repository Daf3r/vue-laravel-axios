<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Noticia;

class NoticiasController extends Controller
{
    public function index()
    {
        return view('noticias.index');
    }

    public function getNoticias()
    {
        $noticias = Noticia::all();
        return response()->json($noticias);
    }
}
