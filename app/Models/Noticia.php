<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    protected $table = 'tbl_noticias';

    protected $filliable = [
        'titulo',
        'descripcion',
        'id_categoria'
    ];
    use HasFactory;

    public function categoria()
    {
        return
            $this->belongsTo(Categoria::class, 'id_categoria');
    }
}